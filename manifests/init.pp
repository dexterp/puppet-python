class python {
  package { 'python':
    ensure => latest
  }
  package { 'pip'}
}